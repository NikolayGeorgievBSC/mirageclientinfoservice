﻿using Cloudtoid.Interprocess;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IpcPublisherTester
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Create the queue factory. If you are not interested in tracing the internals of
            // the queue then don't pass in a loggerFactory

            var queueFactory = new QueueFactory();

            // Create a message queue publisher

            var queueFactoryOptions = new QueueOptions(queueName: "command-queue", bytesCapacity: 1024 * 1024);

            using var publisher = queueFactory.CreatePublisher(queueFactoryOptions);

            Console.Write("Message:");
            var message = Console.ReadLine();
            var stringBytes = Encoding.ASCII.GetBytes(message);
            publisher.TryEnqueue(new ReadOnlySpan<byte>(stringBytes));

            var messageBuffer = new byte[20];
            queueFactoryOptions = new QueueOptions(queueName: "command-queue-response", bytesCapacity: 1024 * 1024);
            using var subscriber = queueFactory.CreateSubscriber(queueFactoryOptions);
            while (true)
            {
                if (subscriber.TryDequeue(messageBuffer, default, out var messageBytes))
                {
                    var response = Encoding.ASCII.GetString(messageBuffer);
                    Console.WriteLine($"Response: {response}");
                    Environment.Exit(0);
                }

                Thread.Sleep(5000);
            }
        }
    }
}
