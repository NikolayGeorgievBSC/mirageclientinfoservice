﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace MirageClientInfoServiceUpdated
{
    class Program
    {
        //private const string fileNameWithoutExtension = "MirageClientInfoService";

        static void Main(string[] args)
        {
            var processId = Convert.ToInt32(args[0]);
            var runningApplication = args[1];
            var downloadedApplication = args[2];

            Console.WriteLine($"Updating started with parameters {processId}, '{runningApplication}' and '{downloadedApplication}'.");

            // Kill MirageClientInfoService if still running
            var processKilled = false;
            var multiplier = 1;
            Process[] processes = Process.GetProcesses();

            while (!processKilled)
            {
                foreach (Process process in processes)
                {
                    if (process.Id == processId)
                    {
                        Console.WriteLine($"Process #{processId} still running.");
                        Console.WriteLine($"Kill process #{processId}.");
                        try
                        {
                            process.Kill();
                            Thread.Sleep(5000);
                            processKilled = true;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine($"Can't kill procces #{processId}! Wait {5000 * multiplier} seconds...");
                            Thread.Sleep(5000 * multiplier);
                            multiplier *= 2;
                        }

                        break;
                    }
                }

                if (multiplier > 512)
                {
                    Environment.Exit(1);
                }
            }

            Console.WriteLine("Copy new files.");
            File.Copy(downloadedApplication, runningApplication, true);

            Console.WriteLine("Start updated service.");
            var serviceStarted = false;
            multiplier = 1;
            while (!serviceStarted)
            {
                try
                {
                    Process.Start(runningApplication);
                    serviceStarted = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Unable to start service! Wait {5000 * multiplier} seconds...");
                    Thread.Sleep(5000 * multiplier);
                }

                if (multiplier > 512)
                {
                    Environment.Exit(1);
                }
            }

            Console.WriteLine("Update complete.");

        }
    }
}
