﻿namespace MirageClientInfoService.Configs
{
    public class WorkerConfig
    {
        public double ProcessInterval { get; set; }
        public string ApiUrl { get; set; }
        public string ApiKey { get; set; }
    }
}
