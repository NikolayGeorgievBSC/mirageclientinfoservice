using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MirageClientInfoService.Configs;
using MirageClientInfoService.Requests;
using System.Data.SqlClient;
using System.Reflection;
using System.Text.Json;
using Cloudtoid.Interprocess;
using System.Text;

namespace MirageClientInfoService
{
    public class Worker : BackgroundService
    {
        private readonly IOptionsMonitor<WorkerConfig> _config;
        private readonly ILogger<Worker> _logger;

        private const string fileNameWithoutExtension = "MirageClientInfoService";

        private bool _externalIpFound = false;
        private bool _internalIpFound = false;
        private bool _hostNameFound = false;
        private bool _osNameFound = false;

        public Worker(IOptionsMonitor<WorkerConfig> config, ILogger<Worker> logger)
        {
            _config = config;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("{time}: Service started.", DateTimeOffset.Now);

            _logger.LogInformation("{time}: Check for update...", DateTimeOffset.Now);
            await CheckForUpdate();

            // create queue factory
            var queueFactory = new QueueFactory();
            var queueFactoryOptions = new QueueOptions(queueName: "command-queue", bytesCapacity: 1024 * 1024);
            using var subscriber = queueFactory.CreateSubscriber(queueFactoryOptions);

            while (!stoppingToken.IsCancellationRequested)
            {
                var messageBuffer = new byte[20];
                try
                {
                    await DoWorkAsync();

                    if (subscriber.TryDequeue(messageBuffer, default, out var messageBytes))
                    {
                        var command = Encoding.ASCII.GetString(messageBuffer);
                        _logger.LogInformation("{time}: Received command: {command}", DateTimeOffset.Now, command);

                        switch (command.Replace("\0", ""))
                        {
                            case "update":
                                await CheckForUpdate();
                                ReturnResponse(queueFactory, "OK");
                                break;
                            case "check license":
                                _logger.LogInformation("{time}: Check license!", DateTimeOffset.Now, command);
                                ReturnResponse(queueFactory, "OK");
                                break;
                            case "close":
                                ReturnResponse(queueFactory, "OK");
                                Environment.Exit(0);
                                break;
                        }
                    }

                }
                catch (Exception e)
                {
                    _logger.LogError(e, e.Message);
                }
                await Task.Delay(TimeSpan.FromSeconds(_config.CurrentValue.ProcessInterval), stoppingToken);
            }

            _logger.LogInformation("{time}: Service stopped.", DateTimeOffset.Now);
        }

        private void ReturnResponse(QueueFactory queueFactory, string message)
        {
            var queueFactoryOptions = new QueueOptions(queueName: "command-queue-response", bytesCapacity: 1024 * 1024);
            using var publisher = queueFactory.CreatePublisher(queueFactoryOptions);

            _logger.LogInformation("{time}: Return response: {message}", DateTimeOffset.Now, message);

            var stringBytes = Encoding.ASCII.GetBytes(message);
            publisher.TryEnqueue(new ReadOnlySpan<byte>(stringBytes));
        }

        private async Task CheckForUpdate()
        {
            var versionResult = await GetUpdateVersion();

            if (versionResult == "")
            {
                return;
            }

            var currentVersion = Assembly.GetExecutingAssembly().GetName().Version;
            var updateVersion = new Version(versionResult.Replace("\"", ""));

            var result = currentVersion.CompareTo(updateVersion);

            if (result < 0) // updateVersion is greater
            {
                _logger.LogInformation("{time}: New version found.", DateTimeOffset.Now);

                _logger.LogInformation("{time}: Downloading update...", DateTimeOffset.Now);
                var userLocalTempPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Temp";
                if (!Directory.Exists(userLocalTempPath + @$"\{fileNameWithoutExtension}"))
                {
                    Directory.CreateDirectory(userLocalTempPath + @$"\{fileNameWithoutExtension}");
                }
                using (var client = new WebClient())
                {
                    client.DownloadFile($"http://tradingsystem-api.brainstorm.bg/update/{fileNameWithoutExtension}.zip", userLocalTempPath + $@"\{fileNameWithoutExtension}\{fileNameWithoutExtension}.exe");
                }

                _logger.LogInformation("{time}: Updating...", DateTimeOffset.Now);
                var process = Process.GetCurrentProcess();
                var fullPath = process.MainModule.FileName;
                SelfUpdate(fullPath, userLocalTempPath + @$"\{fileNameWithoutExtension}\{fileNameWithoutExtension}.exe");
            }
        }

        private void SelfUpdate(string runningApplicationPath, string downloadedApplicationPath)
        {
            var runningApplicationProcessId = Process.GetCurrentProcess().Id.ToString();

            try
            {
                _logger.LogInformation(
                    "{time}: Call updated with parameters {runningApplicationProcessId}, '{runningApplicationPath}' and '{downloadedApplicationPath}'", 
                    DateTimeOffset.Now, runningApplicationProcessId, runningApplicationPath, downloadedApplicationPath);

                Process.Start(
                    $"{fileNameWithoutExtension}Updater.exe",
                    new List<string>() { runningApplicationProcessId, runningApplicationPath, downloadedApplicationPath }
                );
            }
            catch (Exception ex)
            {
                return;
            }

            Environment.Exit(0);
        }

        protected async Task DoWorkAsync()
        {
            var mirageClientInfo = new MirageClientsInfoRequest();

            var dataSent = false;

            var publicIp = "";
            var internalIp = "";
            var hostName = Dns.GetHostName();
            var osName = new OperatingSystem(0, new Version());

            if (!_externalIpFound)
            {
                try
                {
                    publicIp = new WebClient().DownloadString("https://api.ipify.org");
                    _logger.LogInformation("{time}: Public IP: {ip}", DateTimeOffset.Now, publicIp);
                    _externalIpFound = true;
                    mirageClientInfo.ExternalIP = publicIp;
                }
                catch (Exception ex)
                {
                    _logger.LogInformation("Error getting external IP: {reason}", ex.Message);
                }

            }

            if (!_internalIpFound)
            {
                try
                {
                    internalIp = (await Dns.GetHostEntryAsync(hostName)).AddressList.Last().ToString();
                    _logger.LogInformation("{time}: Internal IP: {ip}", DateTimeOffset.Now, internalIp);
                    _internalIpFound = true;
                    mirageClientInfo.InternalIP = internalIp;
                }
                catch (Exception ex)
                {
                    _logger.LogInformation("Error getting internal IP: {reason}", ex.Message);
                }

            }

            if (!_hostNameFound)
            {
                _logger.LogInformation("{time}: Host name: {hostName}", DateTimeOffset.Now, hostName);
                _hostNameFound = true;
                mirageClientInfo.Name = hostName;
            }

            if (!_osNameFound)
            {
                osName = Environment.OSVersion;
                _logger.LogInformation("{time}: OS name: {osName}", DateTimeOffset.Now, osName);
                _osNameFound = true;
                mirageClientInfo.WorkstationOS = osName.ToString();
            }

            var process = Process.GetProcessesByName("miragesql");
            if (process.Length > 0)
            {
                var processPath = Path.GetDirectoryName(process[0].MainModule.FileName);
                var settingsIniData = await File.ReadAllLinesAsync(processPath + "\\settings.ini");

                for (var i = 0; i < settingsIniData.Length; i++)
                {
                    if (settingsIniData[i].Trim().Length > 0)
                    {
                        if (settingsIniData[i] == "[Servers]")
                        {
                            i++;
                            var serversCount = Convert.ToInt32(settingsIniData[i].Split("=")[1]);
                            for (var j = i + 1; j < i + 1 + serversCount; j++)
                            {
                                if (settingsIniData[j].Split("=")[0].StartsWith("ServerAddress"))
                                {
                                    var serverAddress = settingsIniData[j].Split("=")[1];
                                    var regNom = GetRegNomFromDb(serverAddress);
                                    if (regNom != "")
                                    {
                                        _logger.LogInformation("{time}: ReqNom: {reqNom}", DateTimeOffset.Now, regNom);
                                        mirageClientInfo.ServerSqlInstance = serverAddress;
                                        mirageClientInfo.ReqNom = regNom;
                                        break;
                                    }
                                }

                                i++;
                            }
                        }
                    }
                }
            }

            if (!dataSent)
            {
                dataSent = await SendDataToApi(mirageClientInfo);
            }

        }

        private static string Encrypt(string value)
        {
            const string key = "$ecretKey";

            try
            {
                var encryptedJson = SimpleAES.AES256.Encrypt(value, key);

                return encryptedJson;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private async Task<string> GetUpdateVersion()
        {
            try
            {
                var httpClient = new System.Net.Http.HttpClient
                {
                    Timeout = TimeSpan.FromSeconds(60)
                };
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + _config.CurrentValue.ApiKey);
                var result = await httpClient.GetStringAsync(_config.CurrentValue.ApiUrl + "public/api/MirageClientsInfo/CheckServiceVersion");

                _logger.LogInformation("{time}: API result: {result}", DateTimeOffset.Now, result);

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{time}: Can't get version: {result}", DateTimeOffset.Now, ex.Message);
                return "";
            }
        }

        private async Task<bool> SendDataToApi(MirageClientsInfoRequest mirageClientInfo)
        {
            try
            {
                var httpClient = new System.Net.Http.HttpClient
                {
                    Timeout = TimeSpan.FromSeconds(60)
                };
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + _config.CurrentValue.ApiKey);

                var json = JsonSerializer.Serialize(mirageClientInfo);
                var encryptedJson = Encrypt(json);
                var result = await httpClient.PutAsync(
                    _config.CurrentValue.ApiUrl + "public/api/MirageClientsInfo/Add?data=" + encryptedJson, null
                    );

                _logger.LogInformation("{time}: API status code: {result}", DateTimeOffset.Now, result.StatusCode);

                if (result.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{time}: Can't send data: {result}", DateTimeOffset.Now, ex.Message);
                return false;
            }
        }

        private string GetRegNomFromDb(string serverName)
        {
            try
            {

                var regNom = "";

                using (SqlConnection connection = new SqlConnection("Data Source=" + serverName + ";Initial Catalog=Mirage;Persist Security Info=True;User ID=DFLClient;Password=N5BC8KQL1N5BC8KQL1"))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT RegNom FROM dbo.Licence", connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        return reader[0].ToString();
                    }
                    else
                    {
                        return "";
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"{{time}}: SQL connection error: Can't connect to {serverName}", DateTimeOffset.Now, serverName);
                return "";
            }
        }
    }
}
