﻿Install as service: dotnet publish -r win-x64 -c Release

For Windows X64: sc create MirageClientInfoService BinPath=D:\Dev\MirageClientInfoService\MirageClientInfoService\bin\Debug\net5.0\MirageClientInfoService.exe
sc start MirageClientInfoService
sc stop MirageClientInfoService
sc delete MirageClientInfoService

See: https://dotnetcoretutorials.com/2019/12/07/creating-windows-services-in-net-core-part-3-the-net-core-worker-way/

