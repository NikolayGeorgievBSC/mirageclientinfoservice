﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MirageClientInfoService.Configs;

namespace MirageClientInfoService
{
    public static class WorkerConfigurator
    {
        public static void Configure(
            HostBuilderContext hostBuilderContext,
            IServiceCollection services)
        {
            var configuration = hostBuilderContext.Configuration;

            services.AddSingleton(typeof(ILoggerFactory), typeof(LoggerFactory));
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));

            services.Configure<WorkerConfig>(configuration.GetSection("Configs:Worker"));
        }
    }
}
