﻿using System;

namespace MirageClientInfoService.Requests
{
    public class MirageClientsInfoRequest
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public string WindowsSession { get; set; } = "";
        public string ServerName { get; set; } = "";
        public int FirmsCount { get; set; } = 0;
        public int ClientId { get; set; } = 0;
        public int EmployeesCount { get; set; } = 0;
        public int AccountingOperationsCount { get; set; } = 0;
        public string ExternalIP { get; set; } = "";
        public string ServerExternalIP { get; set; } = "";
        public string InternalIP { get; set; } = "";
        public string ServerInternalIP { get; set; } = "";
        public decimal DiskSpace { get; set; } = 0;
        public string ServerOS { get; set; } = "";
        public string WorkstationOS { get; set; } = "";
        public string ServerVersion { get; set; } = "";
        public string WorkstationVersion { get; set; } = "";
        public string SqlserverVersion { get; set; } = "";
        public string ServerSqlInstance { get; set; }
        public string ReqNom { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
    }
}
